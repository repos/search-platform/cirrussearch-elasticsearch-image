#!/bin/bash

set -e
echo Starting opensearch
exec env "discovery.type=single-node" /usr/share/opensearch/opensearch-docker-entrypoint.sh &
sleep 20
curl --silent --fail --insecure \
    -HContent-Type:application/json \
    -XPUT http://localhost:9200/wikidata_index \
    --data @wikidata_index_settings.json > /tmp/resp.json

kill %1

if ! grep '{"acknowledged":true,"shards_acknowledged":true,"index":"wikidata_index"}' /tmp/resp.json; then
	echo "Failed to create a wikidata like index:"
	cat /tmp/resp.json
	exit 1
fi
