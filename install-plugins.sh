#!/bin/sh

set -ex

PLUGINS_URL="https://apt.wikimedia.org/wikimedia/pool/component/opensearch13/w/wmf-opensearch-search-plugins/wmf-opensearch-search-plugins_$PLUGINS_VERSION~bullseye_all.deb"
PLUGINS_DEB="$(ls wmf-opensearch-search-plugins_*.deb 2>/dev/null | head -n 1)"

if [ -f "$PLUGINS_DEB" ]; then
    cp "$PLUGINS_DEB" /tmp/plugins.deb
else
    curl -f -o /tmp/plugins.deb "$PLUGINS_URL"
fi

dpkg-deb -x /tmp/plugins.deb /tmp/plugins
rm /tmp/plugins.deb
