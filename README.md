# CirrusSearch opensearch image

Image suited to maintain CirrusSearch and dependent extensions.

## Content

It is based on the upstream opensearch image with all the plugins that the WMF uses in production.
See [operation/software/plugins](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/software/elasticsearch/plugins/+/refs/heads/master) for the list of plugins.

## Test

The image is tested by creating an index based on the wikidata mappings and settings taken on March 13 2023.

## Building locally

```shell
DOCKER_BUILDKIT=1 docker build --target production --tag docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mylocalbuild  -f .pipeline/blubber.yaml .
```

Or with buildx installed:
```shell
docker buildx build --target production --tag docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mylocalbuild  -f .pipeline/blubber.yaml .
```

Running locally
```shell
docker run --rm -it -e discovery.type=single-node docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mylocalbuild
```

## Testing a new plugin

To create an image with a plugin that is not yet part of the debian package you simply have to drop your plugin zip file
under the `devel_plugins` folder and run the `devel` blubber target:

```shell
DOCKER_BUILDKIT=1 docker build --target devel --tag docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mydevelbuild -f .pipeline/blubber.yaml .
```

Or with buildx installed:

```shell
docker buildx build --target devel --tag docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mydevelbuild  -f .pipeline/blubber.yaml .
```

Then you can update your MW-Docker `docker-compose.override.yaml` file with this image `docker-registry.wikimedia.org/repos/search-platform/cirrussearch-opensearch-image:mydevelbuild`.
