#!/bin/sh

for p in $(find ./devel_plugins -name '*.zip'); do
	./bin/opensearch-plugin install file://$(realpath $p)
done